#!/bin/bash

set -e

git clone https://gitlab.com/everythingfunctional/vegetables.git
cd vegetables
git checkout eda17a1c3d0456a4a47e7dabdff0f67e61e1b38b
git submodule init
git submodule update
./Shakefile.hs
