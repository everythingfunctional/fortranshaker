FROM archlinux/base
RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm base-devel gcc-fortran make valgrind
COPY add-aur.sh /usr/sbin/add-aur
RUN add-aur auruser
RUN su auruser -c "yay -S --noprogressbar --needed --noconfirm stack-bin"
RUN sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
RUN locale-gen
RUN echo "LANG=en_US.UTF-8" > /etc/locale.conf
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
